=========================
Use case
//Admin
1. Quản lí thông tin xe (Thêm, sửa, xóa)
2. Quản lí bãi xe (Thêm, sửa, xóa)
3. Quản lí User (Thêm, sửa, xóa)
//User
4. Tìm kiếm bãi xe
5. Thuê xe
6. Trả xe

======================================
//Job
1. Code
2. Test - Test case, document
3. Diagram 
	+ UseCase diagram
	+ Sequence diagram
4. Screen Transition Diagram
5. Design 
	+ Analysis class
	+ Design class
